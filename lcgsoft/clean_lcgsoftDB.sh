#!/bin/bash -x
#backup of the lcgsoft DB

today=`date +%Y%m%d`

cp /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/public_html/db.backup /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/public_html/db.backup.back$today
cp /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/public_html/db.sqlite3 /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/public_html/db.sqlite.back$today


# sourcing the lcgsoft environment

source /afs/cern.ch/sw/lcg/app/spi/www/lcgsoft/packages/setup.sh

dir=`mkdir $WORKSPACE/jsonfiles`
cd $WORKSPACE/jsonfiles

for slot in dev2 dev3 dev4
do
    $WORKSPACE/lcgjenkins/lcgsoft/ReleaseSummaryReader $slot dummy nightlies
    $WORKSPACE/lcgjenkins/lcgsoft/fill_release.py -f $WORKSPACE/jsonfiles/$slot-dummy.json -d " " -e " " -o "YES"
done
