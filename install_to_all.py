#!/usr/bin/env python
# This script installs created RPMS into the AFS release area

import os, itertools
import getpass
import subprocess
from os import listdir
from os.path import isfile, join, isdir, exists
from optparse import OptionParser
import re
import shutil
import sys, errno
import Tools
from getNewRPMrevision import get_new_RPM_revision
from common_parameters import tmparea, officialarea, INSTALLAREA, INSTALLAREA_READ,INSTALLAREA_VIEWS

def checkAndCopy(rpms, source, target):
    for rpm in rpms:
      print join(target,rpm)
      if exists( join(target,rpm) ):
        print "WARNING:  %s already exists." %(rpm)
      else:
        shutil.copy( join(source,rpm), join(target,rpm) )
        print "  copied %s to %s" %(rpm,target)

def extractNameAndVersionFromRPM(rpm,platform): 
    # Extract name and version from an RPM name of the kind 
    # name-hash_version_platform-1.0.0-revision.noarch.rpm
    # e.g.
    # Frontier_Client-e6888_2.8.10_x86_64_slc6_gcc49_opt-1.0.0-71.noarch.rpm  
    # 
    # name, version and platform (may) contain underscores!
    name_hash_version = rpm.split(platform)[0]
    name = name_hash_version.split("-")[0]
    hash_version = name_hash_version.split("-")[-1]
    version = "_".join(hash_version.split("_")[1:-1])
    return name, version

#########################
if __name__ == "__main__":
    
  # extract command line parameters
  usage = "usage: %prog lcgversion package platform"
  parser = OptionParser(usage)
  parser.add_option('-r', '--revision', action='store', 
                     dest='rpm_revision', help='RPM revision; if not given set to number in LCG release',
                     default="auto"
                    )
  (options, args) = parser.parse_args()    
  if len(args) != 4:
    parser.error("incorrect number of arguments.")
  else:
    release    =  args[0]  # lcg_release
    package    =  args[1]  # package to install
    platform   =  args[2]  # platform being built for
    endsystem  =  args[3]  # end system wehre to install; AFS or CVMFS

  # Massaging of the input parameters
  rpm_platform = platform.replace("-","_")
  if options.rpm_revision == "auto":
    rpm_revision = get_new_RPM_revision(release, rpm_platform)
  else:
    rpm_revision = options.rpm_revision

  # pre-filter all relevant files with matching revision and platform
  new_packagerpms = [ f for f in listdir(tmparea) if f.endswith("%s.noarch.rpm" %rpm_revision) 
                                                 and f.find(rpm_platform) != -1 
                                                 and (f.find("LCG") != 0 or f.find("LCGCMT") == 0)]
  new_linkrpms = [ f for f in listdir(tmparea) if f.endswith("%s.noarch.rpm" %rpm_revision) 
                                                 and f.find(rpm_platform) != -1
                                                 and f.find("LCG") == 0
                                                 and f.find(release) != -1]
  # select the packages to copy and install
  if package not in ("all", "generators"):
    # only look for a single RPM, copy and install it
    packagerpms_to_copy = [f for f in new_packagerpms if (f.lower().split("-")[0] == package.lower() ) ]
    rpms_to_install = packagerpms_to_copy
    linkrpms_to_copy = [] 
    if len(packagerpms_to_copy) != 1:
      print "ERROR: no RPM for package %s found" %package
      sys.exit(errno.EINVAL)
  else:
    #TODO check for consistency of the release; for now take all      
    packagerpms_to_copy = new_packagerpms
    linkrpms_to_copy = new_linkrpms
    release_rpm = "LCG_%s_%s-1.0.0-%s.noarch.rpm" %(release,rpm_platform,rpm_revision)
    generator_rpm = "LCG_generators_%s_%s-1.0.0-%s.noarch.rpm" %(release,rpm_platform,rpm_revision)
    if package == "all":
      rpms_to_install = [release_rpm, generator_rpm]
    else:
      rpms_to_install = [generator_rpm]

  # Copy over --> ONLY FOR AFS
  if "afs" in endsystem:
      print "INFO: Copying over the following RPMs into the official repo:"
      checkAndCopy(packagerpms_to_copy, tmparea, officialarea)
      subdir = "LCG_%srelease" %Tools.extractLCGNumber(release)
      absolute_subdir =  join(officialarea,subdir)
      if not os.path.exists(absolute_subdir):
          os.makedirs(absolute_subdir)
      checkAndCopy(linkrpms_to_copy, tmparea, absolute_subdir)

  # Update RPM database --> ONLY FOR AFS     
      command = "createrepo_c --update --workers=20 %s" %officialarea
      if (Tools.executeCmd(command) == 0):
          print "INFO: Successfully updated RPM database"
      else:
          print "ERROR: could not update RPM database"  
          sys.exit(errno.EINVAL)


  # and install the packages
  if "afs" in endsystem:
      Tools.setEnviron("MYSITEROOT", INSTALLAREA)
      for rpm in rpms_to_install:
#          command = "/afs/cern.ch/sw/lcg/app/spi/tools/LCGRPM/install/lcg_install.sh --rpmupdate install %s" %rpm.rstrip(".noarch.rpm")
          command = "%s/lcgjenkins/LCGRPM/install/lcg_install.sh --rpmupdate install %s" %(os.environ["WORKSPACE"],rpm.rstrip(".noarch.rpm"))
          if (Tools.executeCmd(command) == 0):
              print "INFO: Successfully installed %s." %rpm
          else:
              print "ERROR: could not install %s." %rpm
              sys.exit(errno.EINVAL)
      print "FINISHED INSTALLATION in AFS"
      
      print "STARTING the views creation in AFS"
      command = "python %s/lcgcmake/cmake/scripts/create_lcg_view.py -r %s -l %s -p %s -B %s/LCG_%s/%s" %(os.environ["WORKSPACE"],release,INSTALLAREA_READ,platform,INSTALLAREA_VIEWS,release,rpm_platform)
      if (Tools.executeCmd(command) == 0):
          print "INFO: View in AFS succesfully created"
      else:
          print "ERROR: could not create the VIEW in AFS"
          sys.exit(errno.EINVAL)

      print "Synchronization of READ and WRITE volumes in AFS"
      command = "afs_admin vos_release p.sw.lcg.releases"
      if (Tools.executeCmd(command) == 0):
          print "INFO: Externals AFS area synchronized"
      else:
          print "ERROR: Externals AFS area NOT synchronized"
          sys.exit(errno.EINVAL)
      command = "afs_admin vos_release p.sw.lcg.relmcgen"
      if (Tools.executeCmd(command) == 0):
          print "INFO: Generators AFS area synchronized"
      else:
          print "ERROR: Generators AFS area NOT synchronized"
          sys.exit(errno.EINVAL)
    

  elif "cvmfs" in endsystem:
      command="./lcgjenkins/cvmfs_setup.sh %s %s %s %s" %(release,platform,rpm_revision,package)
      if (Tools.executeCmd(command) == 0):
          print "INFO: Successfully installed to CVMFS and VIEW created"
      else:
          print "ERROR: could not install to CVMFS."
          sys.exit(errno.EINVAL)
      print "FINISHED INSTALLATION in CVMFS"
      
  else:
      print "ERROR: ENDSYSTEM is unknown"
      sys.exit(errno.EINVAL)

